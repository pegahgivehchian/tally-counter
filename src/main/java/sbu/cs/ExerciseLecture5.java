package sbu.cs;

import java.util.Random;

import static java.lang.Character.isDigit;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) { // 97 - 122
        String pass = new String();
        Random rand = new Random();
        for (int i = 0 ; i < length ; i ++){
            int a = rand.nextInt(25) + 97 ;
            pass += (char)(a) ;
        }
        return pass;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception { //33-126
        if (length < 3){
            throw new IllegalValueException();
        }
        StringBuilder password = new StringBuilder();
        Random rand = new Random();
        for (int i = 0 ; i < length ; i ++){
            password.append ((char)(33 + rand.nextInt(94)));
        }
        boolean digit = false ,specialChar = false , alpha = false;
        String allLower = "abcdefghijklmnopqrstuvwxyz";
        //for (char c : password.toCharArray()){
        for (int i = 0 ; i < password.length() ; i ++ ){
            if (isDigit(password.charAt(i))){
                digit = true ;
            }
            else if (allLower.indexOf(Character.toLowerCase(password.charAt(i))) != -1){
                alpha = true ;
            }
            else{
                specialChar = true ;
            }
        }
        if (digit && specialChar && alpha){
            return password.toString();
        }
        return strongPassword(length);
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        ExerciseLecture4 L4 = new ExerciseLecture4() ;
        boolean ans = false ;
       for ( int i = 1 ; i <= n ; i ++){
           if (n == L4.fibonacci(i) + bin((int)L4.fibonacci(i))){
               ans = true ;
               break;
           }
       }
        return ans;
    }
    public static int bin(int dec){
        int numberOfOne = 0 ;
        while (dec > 0){
            if (dec % 2 == 1){
                numberOfOne ++ ;
            }
            dec /= 2 ;
        }
        return numberOfOne ;
    }
}
