package sbu.cs;

public class TallyCounter implements TallyCounterInterface {
    private int cnt ;

    @Override
    public void count() {
        if (cnt < 9999) {
            cnt++;
        }
    }

    @Override
    public int getValue() {
        return cnt;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {
        if (value < 0 || value > 9999){
            throw new IllegalValueException();
        }
        cnt = value ;
    }
}
