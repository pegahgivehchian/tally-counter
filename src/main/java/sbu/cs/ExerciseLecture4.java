package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        int fact = 1 ;
        for (int i = 1 ; i <= n ; i ++ ){
            fact *= i ;
        }
        return fact;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */

    public long fibonacci(int n) {

        if (n == 2 || n == 1){
            return 1 ;
        }
        return fibonacci(n-1) + fibonacci(n-2);
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        StringBuilder word2 = new StringBuilder() ;
        word2.append(word);
        return word2.reverse().toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        String lineNoSpace = line.replace(" ","");
        String lineReverse = reverse(lineNoSpace);
        if (lineReverse.equalsIgnoreCase(lineNoSpace)){
            return true;
        }
        return false;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public static char[][] dotPlot(String str1, String str2) {
        char [][] ans =  makeEmptyArray(str1.length()  , str2.length()  );
        for (int i = 0 ; i < str1.length() ; i ++){
            for (int j = 0 ; j < str2.length() ; j ++){
                if (str1.charAt(i) == str2.charAt(j) ){
                    ans [i][j] = '*';
                }
            }
        }
        return ans;
    }
    static char[][] makeEmptyArray(int dim1, int dim2) {
        char[][] arr = new char[dim1][dim2];
        for (int i = 0; i < dim1; i++) {
            for (int j = 0; j < dim2; j++) {
                arr[i][j] = ' ';
            }
        }
        return arr;
    }
}
