package sbu.cs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long ans = 0 ;
        for (int i = 0 ; i < arr.length ; i += 2){
            ans += arr[i];
        }
        return ans;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int[] ans = new int[arr.length];
        for (int i = arr.length - 1 ; i >= 0 ; i --){
            ans[arr.length - i - 1] = arr[i] ;
        }
        return ans;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        if (m1[0].length != m2.length){
            throw new RuntimeException();
        }
        double [][] ans = new double[m1.length][m2[0].length];
        for (int i = 0 ; i < m1.length ; i ++){
            for (int j = 0 ; j < m2[0].length ; j ++){
                for (int k = 0 ; k < m2.length ; k ++)
                ans[i][j] += m1[i][k] * m2[k][j] ;
            }
        }
        return ans;
    }

    /*
     *   implement a function that returns array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> list1 = new ArrayList<>();
        for (int i = 0 ; i < names.length ; i ++){
            ArrayList<String> list2 = new ArrayList<>();
            for (int j = 0 ; j < names[i].length ; j ++){
                list2.add(names[i][j]);
            }
            list1.add(list2);
        }
        return list1;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        int a = n ;
        List<Integer> ans = new ArrayList<>();
        for (int i = 2 ; i <= a ; i ++){
            if (a%i == 0){
                ans.add(i);
                while (a%i == 0){
                    a /= i ;
                }
            }
        }
        return ans;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        List<String> ans = new ArrayList<>();
        ans = Arrays.asList(line.split("[^a-zA-Z]+"));
        return ans;
    }
}
